
#include <stdio.h>
#include <stdint.h>

#include "kitty_door.h"
#include "person.h"
#include "cat.h"


// Forward Declarations
char PresentScenarioMenu();


// NOTE: The below prototype functions each represent one of the 5 possible alternative scenarios

// HumanPressScenario involves the following objects: Cat, Person, and KittyDoor (which contains a Remote)
// It correlates to the "HumanPress" alternative of the Sequence Diagram
void HumanPressScenario( Cat& cat, Person& person, KittyDoor& kittyDoor );

// MeowRecognizerScenario involves the following objects: Cat, KittyDoor (which contains a SoundRecognizer and a SoundRecognizer contains a MeowRecognizer)
// It correlates to the "MeowRecognizer" alternative of the Sequence Diagram
void MeowRecognizerScenario( Cat& cat, KittyDoor& kittyDoor ); // No Person involved

// MeowRecognizerTimeoutScenario involves the following objects: Cat, KittyDoor (which contains a SoundRecognizer and a SoundRecognizer contains a MeowRecognizer)
// It correlates to the "MeowRecognizer-Timeout" alternative of the Sequence Diagram
void MeowRecognizerTimeoutScenario( Cat& cat, KittyDoor& kittyDoor ); // No Person involved

// MeowRecognizerPawingScenario involves the following objects: Cat, KittyDoor (which contains a PawRecognizer)
// It correlates to the "MeowRecognizer-Pawing" alternative of the Sequence Diagram
void MeowRecognizerPawingScenario( Cat& cat, KittyDoor& kittyDoor ); // No Person involved

// MeowRecognizerPacingScenario involves the following objects: Cat, KittyDoor (which contains a PaceRecognizer)
// It correlates to the "MeowRecognizer-Pacing" alternative of the Sequence Diagram
void MeowRecognizerPacingScenario( Cat& cat, KittyDoor& kittyDoor ); // No Person involved

int main( int argc, char** argv )
{
	// Create Objects
	KittyDoor kittyDoor;
	Person person( kittyDoor.remote );
	Cat cat;

	// Meow()
	
	printf("-------Welcome!\n");

	char repeat;
	do
	{
			printf("-------Please select the scenario to execute.\n");
			printf("-------Each option corresponds to an alternative in the Sequence Diagram.\n");
			printf("-------Simply select from the following menu items...\n\n");

			char choice = PresentScenarioMenu();

			switch( choice )
			{
				case '1':
				{
					printf("-------Executing the HumanPress alternative of sequence diagram...\n");
					HumanPressScenario( cat, person, kittyDoor );
				} break;

				case '2':
				{
					printf("-------Executing the MeowRecognizer alternative of sequence diagram...\n");
					MeowRecognizerScenario( cat, kittyDoor );
				} break;

				case '3':
				{
					printf("-------Executing the MeowRecognizer-Timeout alternative of sequence diagram...\n");
					MeowRecognizerTimeoutScenario( cat, kittyDoor );
				} break;

				case '4':
				{
					printf("-------Executing the MeowRecognizer-Pawing alternative of sequence diagram...\n");
					MeowRecognizerPawingScenario( cat, kittyDoor );
				} break;

				case '5':
				{
					printf("-------Executing the MeowRecognizer-Pawing alternative of sequence diagram...\n");
					MeowRecognizerPacingScenario( cat, kittyDoor );
				} break;

				default:
				{
					printf("ERROR: An invalid choice somehow leaked through!\n");
				} break;
			}


			printf("-------Scenario Ended!\n");

			do
			{
					printf("\n####### Would you like to do another? (y/n)\n");
					printf("####### Valid choices are y and n\n");
					scanf("%c%*c", &repeat);
			} while( repeat != 'y' && repeat != 'n' );
	} while( repeat != 'n' );
	
	return 0;
}

// This is a quick macro to allow for code reuse when acquiring a valid choice in a menu
//  CHOICE: The variable to hold the choice
//  MENU:   The printf's that represent the list to be presented
//  LOWER:  The lower bound of what a valid choice is
// 	UPPER:  The upper bound of what a valid choice is
#define WRAP_VALID_CHOICE( CHOICE, MENU, LOWER, UPPER ) do { MENU; scanf("%c%*c", &CHOICE); } \
								  						while( CHOICE < LOWER || CHOICE > UPPER );

char PresentScenarioMenu()
{
	char choice;

	// Acquire a valid choice
	WRAP_VALID_CHOICE( choice, printf("\n####### Which Scenario would you like to Execute??\n"); \
					   		   printf("####### Valid choices are 1, 2, 3, 4, and 5\n"); \
							   printf("1. HumanPress\n"); \
							   printf("2. MeowRecognizer\n"); \
							   printf("3. MeowRecognizer-Timeout\n"); \
							   printf("4. MeowRecognizer-Pawing\n"); \
							   printf("5. MeowRecognizer-Pacing\n");,
					   '1', '5' );

	return choice;
}

// HumanPressScenario involves the following objects: Cat, Person, and KittyDoor (which contains a Remote)
void HumanPressScenario( Cat& cat, Person& person, KittyDoor& kittyDoor )
{
	// Cat Meows with Meow()
	// Person hears cat with HearCat()
	// Person presses button with PressButton()
	// Remote recognizes button press with ButtonPress()
	// 		Door adjusts based on current state (i.e. If open then close. If closed then open)
	// 		For this scenario, we expect door to be initially closed and thus will be opened
	// 		I.e. Open() will be called
	// Cat recognizes door open and exits with Exit()
	// Cat does business with DoBusiness()
	// Cat returns with Enter()
	// Person notices cat returned with NoticeCatReturned()
	// Person presses button with PressButton()
	// Remote recognizes button press with ButtonPress()
	// 		Door adjusts based on current state (i.e. If open then close. If closed then open)
	// 		For this scenario, we expect door to be still open and thus will be closed
	// 		I.e. Close() will be called
	// 
	
	cat.Meow_HumanPress( person );
}

// MeowRecognizerScenario involves the following objects: Cat, KittyDoor (which contains a SoundRecognizer and a SoundRecognizer contains a MeowRecognizer)
void MeowRecognizerScenario( Cat& cat, KittyDoor& kittyDoor ) // No Person involved
{
	// SCENARIO OVERVIEW - MeowRecognizer
	// 
	// Cat Meows with Meow()
	// KittyDoor's SoundRecognizer recognizes sound with RecognizeSound()
	// The sound is inspected for a Meow with RecognizeMeow()
	// 		The Meow is valid
	// The door is Opened with Open()
	// Cat recognizes door open and exits with Exit()
	// Cat does business with DoBusiness()
	// Cat returns with Enter()
	// Cat Meows with Meow()
	// KittyDoor's SoundRecognizer recognizes sound with RecognizeSound()
	// The sound is inspected for a Meow with RecognizeMeow()
	// 		The Meow is valid
	// The door is Closed with Close()
	// 
	
	cat.Meow_MeowRecognizer( kittyDoor );
}

// MeowRecognizerTimeoutScenario involves the following objects: Cat, KittyDoor (which contains a SoundRecognizer and a SoundRecognizer contains a MeowRecognizer)
void MeowRecognizerTimeoutScenario( Cat& cat, KittyDoor& kittyDoor ) // No Person involved
{
	// SCENARIO OVERVIEW - MeowRecognizer-Timeout
	// 
	// Cat Meows with Meow()
	// KittyDoor's SoundRecognizer recognizes sound with RecognizeSound()
	// The sound is inspected for a Meow with RecognizeMeow()
	// 		The Meow is valid
	// The door is Opened with Open()
	// Cat recognizes door open and exits with Exit()
	// Cat does business with DoBusiness()
	// The door timesout and closes with OpenTimeout()
	// Cat Meows with Meow()
	// KittyDoor's SoundRecognizer recognizes sound with RecognizeSound()
	// The sound is inspected for a Meow with RecognizeMeow()
	// 		The Meow is valid
	// The door is Opened with Open()
	// Cat returns with Enter()
	// The door timesout and closes with OpenTimeout()
	// 
	
	cat.Meow_MeowRecognizer_Timeout( kittyDoor );
}

// MeowRecognizerPawingScenario involves the following objects: Cat, KittyDoor (which contains a PawRecognizer)
void MeowRecognizerPawingScenario( Cat& cat, KittyDoor& kittyDoor ) // No Person involved
{
	// SCENARIO OVERVIEW - MeowRecognizer-Pawing
	// 
	// Cat Meows with Meow()
	// KittyDoor's SoundRecognizer recognizes sound with RecognizeSound()
	// The sound is inspected for a Meow with RecognizeMeow()
	// 		The Meow is valid
	// The door is Opened with Open()
	// Cat recognizes door open and exits with Exit()
	// Cat does business with DoBusiness()
	// The door timesout and closes with OpenTimeout()
	// Cat Paws door with PawDoor()
	// KittyDoor's PawRecognizer recognizes paw with RecognizePaw()
	// The paw is inspected for a valid Paw with RecognizePaw() -> Like a fingerprint sensor
	// 		The Paw is valid
	// The door is Opened with Open()
	// Cat returns with Enter()
	// The door timesout and closes with OpenTimeout()
	// 
	
	cat.Meow_MeowRecognizer_PawDoor( kittyDoor );
}

// MeowRecognizerPacingScenario involves the following objects: Cat, KittyDoor (which contains a PaceRecognizer)
void MeowRecognizerPacingScenario( Cat& cat, KittyDoor& kittyDoor ) // No Person involved
{
	// SCENARIO OVERVIEW - MeowRecognizer-Pacing
	// 
	// Cat Meows with Meow()
	// KittyDoor's SoundRecognizer recognizes sound with RecognizeSound()
	// The sound is inspected for a Meow with RecognizeMeow()
	// 		The Meow is valid
	// The door is Opened with Open()
	// Cat recognizes door open and exits with Exit()
	// Cat does business with DoBusiness()
	// The door timesout and closes with OpenTimeout()
	// Cat Paces by door with PaceByDoor()
	// KittyDoor's PaceRecognizer recognizes pace with RecognizePace()
	// The pace is inspected for a valid Pace with RecognizePace()
	// 		The Pace is valid
	// The door is Opened with Open()
	// Cat returns with Enter()
	// The door timesout and closes with OpenTimeout()
	// 
	
	cat.Meow_MeowRecognizer_PaceByDoor( kittyDoor );
}





