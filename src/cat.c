
#include <stdio.h>

#include "cat.h"

#include "common.h"


void Cat::Meow_HumanPress( Person& person )
{
	Meow();

	if( !person.HearCat() ) // A call to HearCat effectively results in door being opened
	{
		printf("ERROR: Hearing the cat did not result in the door being opened!\n");
	}

	Exit();
	DoBusiness();
	Enter();

	person.NoticeCatReturned();
}

void Cat::Meow_MeowRecognizer( KittyDoor& kittyDoor )
{
	Meow();

	if( !kittyDoor.soundRecognizer.RecognizeSound( VALID_MEOW_STRING ) )
	{
		printf("Recognizing the sound did not result in the door being opened!...\n");
	}

	Exit();
	DoBusiness();
	Enter();

	Meow();

	if( !kittyDoor.soundRecognizer.RecognizeSound( VALID_MEOW_STRING ) )
	{
		printf("Recognizing the sound did not result in the door being opened!...\n");
	}

	kittyDoor.OpenTimeout();

}

void Cat::Meow_MeowRecognizer_Timeout( KittyDoor& kittyDoor )
{
	Meow();

	if( !kittyDoor.soundRecognizer.RecognizeSound( VALID_MEOW_STRING ) )
	{
		printf("Recognizing the sound did not result in the door being opened!...\n");
	}

	Exit();
	DoBusiness();

	kittyDoor.OpenTimeout();

	Meow();

	if( !kittyDoor.soundRecognizer.RecognizeSound( VALID_MEOW_STRING ) )
	{
		printf("Recognizing the sound did not result in the door being opened!...\n");
	}

	Enter();

	kittyDoor.OpenTimeout();

}

void Cat::Meow_MeowRecognizer_PawDoor( KittyDoor& kittyDoor )
{
	Meow();

	if( !kittyDoor.soundRecognizer.RecognizeSound( VALID_MEOW_STRING ) )
	{
		printf("Recognizing the sound did not result in the door being opened!...\n");
	}

	Exit();
	DoBusiness();

	kittyDoor.OpenTimeout();

	PawDoor( kittyDoor );

	Enter();

	kittyDoor.OpenTimeout();

}

void Cat::Meow_MeowRecognizer_PaceByDoor( KittyDoor& kittyDoor )
{
	Meow();

	if( !kittyDoor.soundRecognizer.RecognizeSound( VALID_MEOW_STRING ) )
	{
		printf("Recognizing the sound did not result in the door being opened!...\n");
	}

	Exit();
	DoBusiness();

	kittyDoor.OpenTimeout();

	PaceByDoor( kittyDoor );

	Enter();

	kittyDoor.OpenTimeout();

}

void Cat::Meow()
{
	printf("Meow!\n");
}

void Cat::PawDoor( KittyDoor& kittyDoor )
{
	printf("The Cat PAWS the DOOR...\n");

	if( !kittyDoor.pawRecognizer.RecognizePaw() )
	{
		printf("Recognizing the paw did not result in the door being opened!...\n");
	}
}

void Cat::PaceByDoor( KittyDoor& kittyDoor )
{
	printf("The Cat PACES by the DOOR...\n");

	if( !kittyDoor.paceRecognizer.RecognizePace() )
	{
		printf("Recognizing the pace did not result in the door being opened!...\n");
	}
}

uint8_t Cat::Exit()
{
	printf("The cat EXITS the house through the kitty door...\n");

	if( !SwapLocation( OUTSIDE ) )
	{
		printf("The cat is already OUTSIDE!\n");
	}

	return SUCCESS;
}

uint8_t Cat::DoBusiness()
{
	printf("The cat DOES its BUSINESS...\n");

	return SUCCESS;
}

uint8_t Cat::Enter()
{
	printf("The cat ENTERS the house through the kitty door...\n");

	if( !SwapLocation( INSIDE ) )
	{
		printf("The cat is already INSIDE!\n");
	}

	return SUCCESS;
}




