

#include <stdio.h>
#include <unistd.h> // For sleeping

#include "kitty_door.h"
#include "cat.h"

#include "common.h"



#define DOOR_OPENED SUCCESS
#define DOOR_CLOSED SUCCESS
#define VALID_MEOW SUCCESS


uint8_t KittyDoor::Remote::ButtonPress()
{
	printf("The remote received a BUTTON PRESS...\n");

	if( kittyDoor.state == OPEN )
		kittyDoor.Close();
	else if( kittyDoor.state == CLOSED )
		kittyDoor.Open();

	printf("The DOOR is now %s...\n", kittyDoor.state == OPEN ? "OPEN" : "CLOSED");

	return SUCCESS;
}

uint8_t KittyDoor::Open()
{
	printf("OPENING the DOOR...\n");

	state = OPEN;

	return SUCCESS;
}

uint8_t KittyDoor::OpenTimeout()
{
	printf("OPEN TIMEOUT started... Waiting for %i seconds...\n", openTimeout);

	DoorTickerDelay( openTimeout );

	printf("TIMEOUT EXPIRED...\n");

	Close();

	return DOOR_CLOSED;
}

uint8_t KittyDoor::Close()
{
	printf("CLOSING the DOOR...\n");

	state = CLOSED;

	return SUCCESS;
}


uint8_t KittyDoor::SoundRecognizer::RecognizeSound( const char* soundReceived )
{
	printf("A SOUND was RECOGNIZED...\n");
	
	if( !meowRecognizer.RecognizeMeow( soundReceived ) )
	{
		printf("Received SOUND was NOT VALID!...\n");

		return FAILURE;
	}

	printf("Received SOUND was VALID...\n");

	if( kittyDoor.state == OPEN )
		kittyDoor.Close();
	else if( kittyDoor.state == CLOSED )
		kittyDoor.Open();

	return kittyDoor.state == OPEN ? DOOR_OPENED : DOOR_CLOSED;
}

uint8_t StringCompare( const char* stringOne, uint8_t stringOneLength,
					   const char* stringTwo, uint8_t stringTwoLength )
{
	if( stringOneLength != stringTwoLength )
		return FAILURE;

	for( uint8_t charCounter = 0; charCounter < stringOneLength; charCounter++ )
	{
		if( stringOne[charCounter] != stringTwo[charCounter] )
			return FAILURE;
	}

	return SUCCESS;
}

uint8_t StringLength( const char* stringToInspect )
{
	uint8_t result = 0;
	while( *(stringToInspect++) != '\0' )
		result++;

	return result;
}

uint8_t KittyDoor::SoundRecognizer::MeowRecognizer::RecognizeMeow( const char* meowReceived )
{
	printf("A MEOW was RECOGNIZED...\n");

	if( !StringCompare( meowReceived, StringLength( meowReceived ), 
						validMeow, StringLength( validMeow ) ) )
	{
		printf("Received MEOW was NOT VALID...\n");
		return FAILURE;
	}

	printf("Received MEOW was VALID...\n");

	return VALID_MEOW;
}

uint8_t KittyDoor::PawRecognizer::RecognizePaw()
{
	printf("A PAW was RECOGNIZED...\n");

	printf("Received PAW was VALID...\n");

	kittyDoor.Open();

	return DOOR_OPENED;
}

uint8_t KittyDoor::PaceRecognizer::RecognizePace()
{
	printf("A PACE was RECOGNIZED...\n");

	printf("Received PACE was VALID...\n");

	kittyDoor.Open();

	return DOOR_OPENED;
}





