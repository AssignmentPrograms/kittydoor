
#include <stdio.h>

#include "person.h"

#include "common.h"


#define DOOR_OPENED SUCCESS


uint8_t Person::HearCat()
{
	printf("The person HEARS the CAT...\n");

	PressButton();

	return DOOR_OPENED;
}

uint8_t Person::PressButton()
{
	printf("The person PRESSES the BUTTON...\n");

	if( !kittyDoorRemote.ButtonPress() )
	{
		printf("ERROR: The ButtonPress failed!\n");
		return FAILURE;
	}

	return SUCCESS;
}

uint8_t Person::NoticeCatReturned()
{
	PressButton();
	
	return SUCCESS;
}



