
#ifndef PERSON_H
#define PERSON_H

#include <stdint.h>

#include "kitty_door.h"

class Person
{
	public:
		Person( KittyDoor::Remote& kittyDoorRemoteParent ) : kittyDoorRemote( kittyDoorRemoteParent ) {}

		uint8_t HearCat();
		uint8_t PressButton();
		uint8_t NoticeCatReturned();

		~Person() {}

	private:
		// Made as a reference since I want this Remote to be the Remote used in the original KittyDoor
		KittyDoor::Remote& kittyDoorRemote;
};




#endif // PERSON_H
