

#ifndef KITTY_DOOR_H
#define KITTY_DOOR_H


#include <stdint.h>
#include <time.h>


#define OPEN_TIMEOUT 5

#define VALID_MEOW_STRING "Meow"


enum DoorState
{
	OPEN,
	CLOSED
};

class KittyDoor
{
	public:
		// Forward Declarations
		class Remote; 
		class SoundRecognizer;

		// Methods
		KittyDoor() : remote( *this ), soundRecognizer( *this ), pawRecognizer( *this ), paceRecognizer( *this ), state( CLOSED ), openTimeout( OPEN_TIMEOUT ) {}

		uint8_t Open();
		uint8_t OpenTimeout();
		uint8_t Close();

		~KittyDoor() {}

		class Remote // Remote object being represented as a has-a relationship (nested class)
		{
			public:
				Remote( KittyDoor& kittyDoorParent ) : kittyDoor( kittyDoorParent ) {}

				uint8_t ButtonPress();

				~Remote() {}
			
			private:
				KittyDoor& kittyDoor;
		} remote;

		class SoundRecognizer
		{
			public:
				// Forward Declarations
				class MeowRecognizer;

				// Methods
				SoundRecognizer( KittyDoor& kittyDoorParent ) : meowRecognizer( *this ), kittyDoor( kittyDoorParent ) {}

				uint8_t RecognizeSound( const char* soundReceived );

				~SoundRecognizer() {}

				class MeowRecognizer
				{
					public:
						MeowRecognizer( SoundRecognizer& soundRecognizerParent ) : soundRecognizer( soundRecognizerParent ) {}

						uint8_t RecognizeMeow( const char* meowReceived );

						~MeowRecognizer() {}

					private:
						SoundRecognizer& soundRecognizer;
						const char* validMeow = VALID_MEOW_STRING;
				} meowRecognizer;
			private:
				KittyDoor& kittyDoor;
		} soundRecognizer;

		class PawRecognizer
		{
			public:
				// Methods
				PawRecognizer( KittyDoor& kittyDoorParent ) : kittyDoor( kittyDoorParent ) {}

				uint8_t RecognizePaw();

				~PawRecognizer() {}

			private:
				KittyDoor& kittyDoor;
		} pawRecognizer;

		class PaceRecognizer
		{
			public:
				// Methods
				PaceRecognizer( KittyDoor& kittyDoorParent ) : kittyDoor( kittyDoorParent ) {}

				uint8_t RecognizePace();

				~PaceRecognizer() {}

			private:
				KittyDoor& kittyDoor;
		} paceRecognizer;

	private:
		DoorState state;
		uint32_t openTimeout;

		// Private method that the OpenTimeout uses to cause a delay
		inline void DoorTickerDelay( uint32_t delaySeconds )
		{
			// How many clock ticks to wait?
			long delayClocks = delaySeconds * CLOCKS_PER_SEC;
			clock_t now, then;


			now = then = clock();

			while( (now-then) < delayClocks )
					now = clock();
		}
};




#endif // KITTY_DOOR_H
