
#ifndef CAT_H
#define CAT_H

#include <stdint.h>

#include "person.h"

#include "common.h"

#define START_LOCATION INSIDE

class Cat
{
	public:
		Cat() : location( START_LOCATION ) {}

		// The different scenarios
		// 	This is to remain consistant with the sequence diagrams of a Cat object remaining alive throughout the scenario
		void Meow_HumanPress( Person& person );
		void Meow_MeowRecognizer( KittyDoor& kittyDoor );
		void Meow_MeowRecognizer_Timeout( KittyDoor& kittyDoor );
		void Meow_MeowRecognizer_PawDoor( KittyDoor& kittyDoor );
		void Meow_MeowRecognizer_PaceByDoor( KittyDoor& kittyDoor );

		void Meow();
		void PawDoor( KittyDoor& kittyDoor );
		void PaceByDoor( KittyDoor& kittyDoor );

		uint8_t Exit();
		uint8_t DoBusiness();
		uint8_t Enter();

		~Cat() {}

		enum Location
		{
			INSIDE = 0, // Numbered manually to show intent of using !location to swap location
			OUTSIDE = 1
		} location;

		inline uint8_t SwapLocation( Location desiredLocation )
		{
			// If the cat's location is already the desired location, then there is no swapping needed
			// In this case, swapping locations failed and FAILURE is returned so caller can handle as desired
			if( location == desiredLocation )
			{
				printf("The cat is already in the desired location!\n");
				return FAILURE;
			}

			location = (Location) !location; // Cast to allow use of boolean operator !

			return SUCCESS;
		}

	private:
};




#endif // CAT_H
