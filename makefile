

SRCDIR =src
INCDIR =inc
OBJDIR =obj

CC =g++
CFLAGS =-Wall -I$(INCDIR)

OUT =run

MAIN =main.c

_DEPS =kitty_door.h person.h cat.h

DEPS =$(patsubst %, $(INCDIR)/%, $(_DEPS))
SOURCES =$(SRCDIR)/$(MAIN) $(subst .h,.c, $(patsubst %, $(SRCDIR)/%, $(_DEPS)))
OBJECTS =$(SRCDIR)/$(subst .c,.o,$(MAIN)) $(subst .h,.o, $(patsubst %, $(OBJDIR)/%, $(_DEPS)))


$(OBJDIR)/%.o: $(SRCDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(OUT): $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS)

help:
	echo sources = $(SOURCES)
	echo objects = $(OBJECTS)
	echo dependencies = $(DEPS)
	echo command = $(CC) $(CFLAGS) $(SOURCES)


